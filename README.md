# Install this dotfiles to a new system
* add the line `alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'` to `.bashrc`, `.zshrc` or `config.fish`
* execute `echo ".cfg" >> .gitignore` in source folder for repo ($HOME)
```bash
git clone --bare git@gitlab.com:mfleck/dotfiles.git $HOME/.cfg
config checkout
config config --local status.showUntrackedFiles no
```
* use different branches for different computers

# Starting from scratch
see https://www.atlassian.com/git/tutorials/dotfiles for complete explanation
```bash
git init --bare $HOME/.cfg
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
config config --local status.showUntrackedFiles no
echo "alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'" >> $HOME/.bashrc
```
