config.load_autoconfig(True)

c.auto_save.session = True
c.session.lazy_restore = False
c.spellcheck.languages = ['en-US']
c.tabs.background = True
c.tabs.last_close = 'blank'
c.tabs.mousewheel_switching = False
c.tabs.pinned.frozen = True
c.tabs.position = 'left'
c.tabs.show = 'switching'
c.tabs.show_switching_delay = 1200
c.tabs.title.alignment = 'left'
c.url.open_base_url = False
# c.url.start_pages = ['https://outlook.office.com/calendar/view/workweek']
# c.url.default_page = 'https://outlook.office.com/calendar/view/workweek'
c.window.hide_decoration = False

c.url.searchengines['DEFAULT'] = 'https://www.google.com/search?source=hp&ei=6_ylX4egHpO7lwTdxI2YAg&q={}'
c.url.searchengines['gde'] = 'https://www.google.de/search?source=hp&ei=6_ylX4egHpO7lwTdxI2YAg&q={}'
c.url.searchengines['maps'] = 'https://www.google.com/maps?q={}'
c.url.searchengines['bilder'] = 'https://www.google.com/search?tbm=isch&q={}'
c.url.searchengines['wiki'] = 'https://en.wikipedia.org/wiki/{}'
c.url.searchengines['enwiki'] = 'https://en.wikipedia.org/wiki/{}'
c.url.searchengines['dewiki'] = 'https://de.wikipedia.org/wiki/{}'
c.url.searchengines['youtube'] = 'https://www.youtube.com/results?search_query={}'
c.url.searchengines['dict'] = 'https://www.dict.cc/?s={}'
c.url.searchengines['leo'] = 'https://dict.leo.org/german-english/{}'
c.url.searchengines['amazon'] = 'https://smile.amazon.de/s?k={}'
c.url.searchengines['band'] = 'https://bandcamp.com/search?q={}'
c.url.searchengines['apt'] = 'https://packages.debian.org/source/sid/{}'
c.url.searchengines['ebay'] = 'https://www.ebay.de/sch/i.html?_nkw={}&_ipg=200&_sop=15' # see http://www.helios825.org/url-parameters.php for parameter explanation
c.url.searchengines['ebayk'] = 'https://www.ebay-kleinanzeigen.de/s-{}/k0'
c.url.searchengines['rym'] = 'https://rateyourmusic.com/search?&searchterm={}'
c.url.searchengines['sputnik'] = 'https://www.sputnikmusic.com/search_results.php?&search_text={}&x=0&y=0'
c.url.searchengines['thomann'] = 'https://www.thomann.de/intl/search_dir.html?sw={}'
c.url.searchengines['commons'] = 'http://commons.wikimedia.org/w/index.php?title=Special:Search&search={}'
c.url.searchengines['quote'] = 'http://de.wikiquote.org/wiki/{}'
c.url.searchengines['enquote'] = 'http://en.wikiquote.org/wiki/{}'
c.url.searchengines['wa'] = 'http://www.wolframalpha.com/input/?i={}'
c.url.searchengines['idealo'] = 'https://www.idealo.de/preisvergleich/MainSearchProductCategory.html?q={}'
c.url.searchengines['ali'] = 'https://de.aliexpress.com/wholesale?&SearchText={}'
c.url.searchengines['d'] = 'https://duckduckgo.com/?q={}&ia=web'

