alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
alias ls='ls -Alh --color=auto'
alias vim=nvim

export QT_QPA_PLATFORMTHEME=qt5ct
export QT_QPA_PLATFORM=wayland

